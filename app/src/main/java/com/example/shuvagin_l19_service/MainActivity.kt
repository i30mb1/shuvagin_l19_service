package com.example.shuvagin_l19_service

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.whenResumed
import com.example.shuvagin_l19_service.ui.main.MainFragment
import com.example.shuvagin_l19_service.utils.enableStrictMode
import com.example.shuvagin_l19_service.utils.log
import kotlinx.coroutines.*
import kotlinx.coroutines.android.awaitFrame

// можно делац вот так
class MainActivity : AppCompatActivity(), CoroutineScope by (CoroutineScope(Dispatchers.IO)) {

    override fun onCreate(savedInstanceState: Bundle?) {
        enableStrictMode()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        launch {
            whenResumed {
                while (true) {
                    delay(1000)
                    awaitFrame()
                    log("working...")
                }
            }
        }
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cancel()
    }
}
